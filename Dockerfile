# Image for Duniter releases on Linux.
#
# Building this image:
#   docker build . -t duniter/release-builder

FROM ubuntu:14.04

ENV DEBIAN_FRONTEND noninteractive

# Install needed tools
RUN apt-get update && \
	apt-get install -y apt-transport-https curl && \ 
	apt-get update && \
	apt-get install -y libxcb1 libxcb1-dev libx11-xcb1 libx11-xcb-dev libxcb-keysyms1 libxcb-keysyms1-dev libxcb-image0 \
            libxcb-image0-dev libxcb-shm0 libxcb-shm0-dev libxcb-icccm4 libxcb-icccm4-dev \
            libxcb-xfixes0-dev libxrender-dev libxcb-shape0-dev libxcb-randr0-dev libxcb-render-util0 \
            libxcb-render-util0-dev libxcb-glx0-dev libgl1-mesa-dri libegl1-mesa libpcre3 libgles2-mesa-dev \
            freeglut3-dev libfreetype6-dev xorg-dev xserver-xorg-input-void xserver-xorg-video-dummy xpra libosmesa6-dev \
            curl libdbus-1-dev libdbus-glib-1-dev autoconf automake libtool libgstreamer-plugins-base0.10-0 dunst fakeroot \
            dbus-x11 git gcc make libsqlite3-dev libbz2-dev libssl-dev libreadline6-dev xvfb && \
	apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Create compiling user
RUN mkdir /builder && \
	adduser --system --group --quiet --shell /bin/bash --home /builder builder && \
	chown builder:builder /builder
WORKDIR /builder

# Load libsodium
USER root
RUN curl https://download.libsodium.org/libsodium/releases/LATEST.tar.gz --output LATEST.tar.gz && \
    tar zxvf LATEST.tar.gz && \
    cd libsodium-stable && \
    ./configure && \
    make && \
    make check && \
    make install && \
    cd .. && \
    rm -rf libsodium-stable

# Load pyenv
ENV PYENV_ROOT "/builder/.pyenv"
USER builder
RUN git clone https://github.com/pyenv/pyenv.git /builder/.pyenv && \
    export PATH=$PYENV_ROOT/bin:$PATH && \
    PYTHON_CONFIGURE_OPTS="--enable-shared" pyenv install --force 3.5.5 && \
    PYTHON_CONFIGURE_OPTS="--enable-shared" pyenv install --force 3.6.4

USER root
ADD xvfb.sh /etc/init.d/xvfb
RUN chmod a+x /etc/init.d/xvfb
ADD xvfb-run.sh /usr/bin/xvfb-run
RUN chmod a+x /usr/bin/xvfb-run


USER root
COPY qt-installer-noninteractive.qs /root/qt-installer-noninteractive.qs
RUN export XVFBARGS="-screen 0 1280x1024x24" && \
    export DISPLAY=:99  && \
    /etc/init.d/xvfb start && \
    sleep 3  && \
    curl -L https://download.qt.io/official_releases/qt/5.9/5.9.4/qt-opensource-linux-x64-5.9.4.run --output qt-opensource-linux-x64-5.9.4.run && \
    chmod +x qt-opensource-linux-x64-5.9.4.run && \
    ./qt-opensource-linux-x64-5.9.4.run -v --script /root/qt-installer-noninteractive.qs
    
# Entry point
USER root
COPY bootstrap.sh /root/bootstrap.sh
RUN chmod u+x /root/bootstrap.sh
ENTRYPOINT ["/root/bootstrap.sh"]
