#!/bin/bash

# Test environment
if [[ ! -d /builds ]]; then
	echo "In order to use this image, you must mount the working directory to /builds"
	exit 1
fi
REFERENCE=$(ls -d /builds/* | head -1)
if [[ -z "${REFERENCE}" ]]; then
	echo "Aborting because of empty working directory"
	exit 1
fi

# Prepare environment
_builder_prepare() {
	touch /builder/reference || exit 1
	chown --reference "${REFERENCE}" /builder/reference || exit 1
	chown -R builder:builder /builds/* || exit 1
	export XVFBARGS="-screen 0 1280x1024x24"
    export DISPLAY=:99
    /etc/init.d/xvfb start
}

# Terminate
_builder_terminate() {
	chown -R --reference /builder/reference /builds/* || exit 1
	rm -f /builder/reference
	exit ${1}
}

# Prepare and execute building
_builder_prepare
su - builder
_builder_terminate $?
